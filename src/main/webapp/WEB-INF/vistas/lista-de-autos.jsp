<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: ezequiel
  Date: 7/10/21
  Time: 06:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/planes.css">
    <link rel="icon" href="img/favicon.ico" type="image/png" />
    <title>Proyecto - Alquiler de autos</title>
</head>
<body>
<header class="d-flex flex-row-reverse p-3">

</header>
<section class="bg-light">
    <div class="container text-center">
        <div>
            <h1 class="display-4 p-4">Lista de Autos</h1>
        </div>
    </div>
</section>
<section class="bg-light">
    <div class="pricing6 py-5">
        <div class="container">
            <div class="col center-block">
                <c:forEach items="${autosEnMantenimiento}" var="autos">
                    <div class="col-md-12">
                        <div class="card card-shadow border-0 mb-4">
                            <div class="card-body p-4">
                                <div class="d-flex align-items-center">
                                    <h5 class="font-weight-medium mb-0">Auto: (Iria Patente o algo)</h5>
                                    <div class="ml-auto"><span class="badge badge-danger font-weight-normal p-2">Mas alquilado</span>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="col-lg-12">
                                        <div class="row mt-3">
                                            <div class="col-lg-6 align-self-center">
                                                <ul class="list-inline pl-3 font-16 font-weight-medium text-dark mt-3">
                                                    <li class="py-2"><i class="icon-check text-info mr-2"></i> <span>${autos.patente}</span>
                                                    </li>
                                                    <li class="py-2"><i class="icon-check text-info mr-2"></i> <span>${autos.marca}</span>
                                                    </li>
                                                    <li class="py-2"><i class="icon-check text-info mr-2"></i> <span>${autos.modelo}</span>
                                                    </li>
                                                    <li class="py-2"><i class="icon-check text-info mr-2"></i> <span>${autos.km}</span>

                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="container">
                                                    <img src="https://www.automobilemag.com/uploads/sites/11/2014/02/2015-Ford-Focus-hatchback-front-view2.jpg?fit=around%7C875:492"
                                                         alt="" style="width: 100%; height: auto;">
                                                </div>
                                            </div>
                                            <a class="btn btn-info-gradiant font-14 border-0 text-white p-3 btn-block mt-3"
                                               href="#">ENVIAR A MANTENIMIENTO</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </c:forEach>
                <!-- row  -->
                <h2>${mensaje}</h2>
                <!-- column  -->

                <!-- column  -->
                <!-- column  -->
                <div class="col-md-12">
                    <div class="card card-shadow border-0 mb-4">
                        <div class="card-body p-4">
                            <div class="d-flex align-items-center">
                                <h5 class="font-weight-medium mb-0">Auto: (Iria Patente o algo)</h5>
                                <div class="ml-auto"><span class="badge badge-danger font-weight-normal p-2">Mas alquilado</span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="col-lg-12">
                                    <div class="row mt-3">
                                        <div class="col-lg-6 align-self-center">
                                            <ul class="list-inline pl-3 font-16 font-weight-medium text-dark mt-3">
                                                <li class="py-2"><i class="icon-check text-info mr-2"></i> <span>Patente: BBB111</span>
                                                </li>
                                                <li class="py-2"><i class="icon-check text-info mr-2"></i> <span>Marca: Chevrolet</span>
                                                </li>
                                                <li class="py-2"><i class="icon-check text-info mr-2"></i> <span>Modelo: Onix</span>
                                                </li>
                                                <li class="py-2"><i class="icon-check text-info mr-2"></i> <span>Kilomtraje Total: 150km</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="container">
                                                <img src="https://3.bp.blogspot.com/-2S9cuFUrN7I/XKfwZuAAATI/AAAAAAAAdEc/TbKdrRbdwXkTCNy3-deFvJdESVCkA5VngCLcBGAs/s1600/Ficha-Tecnica-Chevrolet-Onix-2019.jpg"
                                                     alt="" style="width: 100%; height: auto;">
                                            </div>
                                        </div>
                                        <a class="btn btn-info-gradiant font-14 border-0 text-white p-3 btn-block mt-3"
                                           href="#">ENVIAR A MANTENIMIENTO</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card card-shadow border-0 mb-4">
                        <div class="card-body p-4">
                            <div class="d-flex align-items-center">
                                <h5 class="font-weight-medium mb-0">Auto: (Iria Patente o algo)</h5>
                                <div class="ml-auto"><span class="badge badge-danger font-weight-normal p-2">Mas alquilado</span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="col-lg-12">
                                    <div class="row mt-3">
                                        <div class="col-lg-6 align-self-center">
                                            <ul class="list-inline pl-3 font-16 font-weight-medium text-dark mt-3">
                                                <li class="py-2"><i class="icon-check text-info mr-2"></i> <span>Patente: CCC111</span>
                                                </li>
                                                <li class="py-2"><i class="icon-check text-info mr-2"></i> <span>Marca: Volkswagen</span>
                                                </li>
                                                <li class="py-2"><i class="icon-check text-info mr-2"></i> <span>Modelo: Gol Trend</span>
                                                </li>
                                                <li class="py-2"><i class="icon-check text-info mr-2"></i> <span>Kilomtraje Total: 150km</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="container">
                                                <img src="https://acroadtrip.blob.core.windows.net/catalogo-imagenes/xl/RT_V_bde57fdcf34b4e75be144e7400b4c97a.jpg"
                                                     alt="" style="width: 100%; height: auto;">
                                            </div>
                                        </div>
                                        <a class="btn btn-info-gradiant font-14 border-0 text-white p-3 btn-block mt-3"
                                           href="#">ENVIAR A MANTENIMIENTO</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- column  -->
            </div>
        </div>
    </div>
</section>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>


</body>
<jsp:include page="footer.jsp" />
</html>